const express = require('express');
const routes = require('./routes');

const app = express();
const PORT = process.env.PORT || 3000;
const knex = require('./config/db');

// knex('phases')
//   .insert({ phaseName: 'New Phase', phaseDesc: 'stuff', user_id: 3 })
//   .then(() => console.log('inserted'))
//   .catch(err => err);

knex
  .select()
  .table('phases')
  .where('user_id', 3)
  .then(users => console.log(users));

// MIDDLEWARE
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(express.static('public'));
app.use(routes);
// app.use('/', htmlRogit utes);

app.listen(PORT, () => console.log(`Listening on PORT: ${PORT}`));
